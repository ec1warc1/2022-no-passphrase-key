#!/bin/bash
# Created by Edward Stoever for MariaDB Support
if [ -f ~/.ssh/2022-no-passphrase ]; then echo "It appears that this script has been run already. Exiting."; exit 0; fi
echo "This script will install a keypair for passwordless ssh between servers."
echo "Type c to continue running the script."
echo "Type any other key to exit. "
read -s -n 1 RESPONSE
if [ ! "$RESPONSE" = "c" ]; then
  printf "================\nSCRIPT NOT RUN.\n"; exit 0;
else
  printf "\n";
fi

if [[ ! $(which unzip) ]]; then echo "unzip is not installed. Exiting"; exit 0; fi

mkdir -p ~/.ssh
chmod 700 ~/.ssh
touch ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys

cd $(dirname "$0")
unzip -o 2022-no-passphrase.zip || echo "Something went wrong in the unzip operation."
cp 2022-no-passphrase ~/.ssh/
cat 2022-no-passphrase.pub >> ~/.ssh/authorized_keys

FILE=$(find ~/ -type f \( -name .profile -o -name .bash_profile \)| head -1)
if [ $(grep 'start ssh agent on login' $FILE | head -1) ]; then
  echo "check $FILE to verify that agent is run from it."
else
  cat profile.snip.txt >> $FILE
  # source profile
  echo "Start the agent by sourcing $FILE or logout and login."
fi

